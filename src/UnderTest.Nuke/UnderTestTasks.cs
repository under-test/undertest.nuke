using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using Nuke.Common.Tooling;

namespace UnderTest.Nuke
{
  [PublicAPI]
  [ExcludeFromCodeCoverage]
  public static class UnderTestTasks
  {
    public static Action<OutputType, string> UnderTestLogger { get; set; } = ProcessTasks.DefaultLogger;

    public static IReadOnlyCollection<Output> UnderTestRunSuite(UnderTestRunSuiteSettings toolSuiteSettings = null)
    {
      toolSuiteSettings = toolSuiteSettings ?? new UnderTestRunSuiteSettings();
      var process = ProcessTasks.StartProcess(toolSuiteSettings);
      process.AssertZeroExitCode();
      return process.Output;
    }

    public static IReadOnlyCollection<Output> UnderTestRunSuite(Configure<UnderTestRunSuiteSettings> configurator)
    {
      return UnderTestRunSuite(configurator(new UnderTestRunSuiteSettings()));
    }

    public static IEnumerable<(UnderTestRunSuiteSettings Settings, IReadOnlyCollection<Output> Output)> UnderTestRunSuite(CombinatorialConfigure<UnderTestRunSuiteSettings> configurator, int degreeOfParallelism = 1, bool completeOnFailure = false)
    {
      return configurator.Invoke(UnderTestRunSuite, UnderTestLogger, degreeOfParallelism, completeOnFailure);
    }
  }
}
