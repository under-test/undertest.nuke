using JetBrains.Annotations;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;

namespace UnderTest.Nuke
{
  [PublicAPI]
  public static class UnderTestRunSettingsExtensions
  {
    public static UnderTestRunSuiteSettings SetDoNotRunTheseTags(this UnderTestRunSuiteSettings toolSuiteSettings, params string[] tags)
    {
      toolSuiteSettings = toolSuiteSettings.NewInstance();
      toolSuiteSettings.DoNotRunTheseTagsInternal.AddRange(tags);
      return toolSuiteSettings;
    }

    public static UnderTestRunSuiteSettings SetConfiguration(this UnderTestRunSuiteSettings toolSettings,
      string configuration)
    {
      toolSettings = toolSettings.NewInstance();
      toolSettings.Configuration = configuration;
      return toolSettings;
    }

    public static UnderTestRunSuiteSettings SetNoBuild(this UnderTestRunSuiteSettings toolSettings,
      bool noBuild)
    {
      toolSettings = toolSettings.NewInstance();
      toolSettings.NoBuild = noBuild;
      return toolSettings;
    }

    public static UnderTestRunSuiteSettings SetExecutionMode(this UnderTestRunSuiteSettings toolSettings,
      DotNetExecutionMode mode)
    {
      toolSettings = toolSettings.NewInstance();
      toolSettings.ExecutionMode = mode;
      return toolSettings;
    }

    public static UnderTestRunSuiteSettings SetOnlyRunTheseTags(this UnderTestRunSuiteSettings toolSuiteSettings, string[] tags)
    {
      toolSuiteSettings = toolSuiteSettings.NewInstance();
      toolSuiteSettings.OnlyRunTheseTagsInternal.AddRange(tags);
      return toolSuiteSettings;
    }

    public static UnderTestRunSuiteSettings SetProject(this UnderTestRunSuiteSettings toolSuiteSettings, Project project)
    {
      toolSuiteSettings = toolSuiteSettings.NewInstance();
      toolSuiteSettings.ProjectFile = project.Path;
      return toolSuiteSettings;
    }

    public static UnderTestRunSuiteSettings SetProjectFile(this UnderTestRunSuiteSettings toolSuiteSettings, string projectFile)
    {
      toolSuiteSettings = toolSuiteSettings.NewInstance();
      toolSuiteSettings.ProjectFile = projectFile;
      return toolSuiteSettings;
    }

    public static UnderTestRunSuiteSettings SetReportOutputFolder(this UnderTestRunSuiteSettings toolSuiteSettings, string folderPath)
    {
      toolSuiteSettings = toolSuiteSettings.NewInstance();
      toolSuiteSettings.ReportOutputFolder = folderPath;
      return toolSuiteSettings;
    }

    public static UnderTestRunSuiteSettings ReportOutputFileName(this UnderTestRunSuiteSettings toolSuiteSettings, string fileName)
    {
      toolSuiteSettings = toolSuiteSettings.NewInstance();
      toolSuiteSettings.ReportOutputFileName = fileName;
      return toolSuiteSettings;
    }
  }
}
