# UnderTest.Nuke

In building [UnderTest](https://gitlab.com/under-test/undertest) we were copying runner code from project to project and this was creating technical debt. 
From this, we built **UnderTest.Nuke**, which takes our opinions on how BDD acceptance tests should be automated.  
We built this with a few principles:

- Quick to get started
- Repeatable pattern to the builds
- Report generation is critical to success

## Getting Started:

### Only running acceptance tests

In many projects, there are solutions only setup for running tests.  These may be tests which span multiple microservices or systems.  For situations like these, we have our `UnderTestBuildRunner` which will template out the entire build process and get to your test results as quickly as possible. 

1. Create your acceptance testing project with [UnderTest](https://gitlab.com/under-test/undertest)
2. Add a build project using [nuke.build](http://www.nuke.build/)’s `nuke :setup --boot`
3. Add UnderTest.Nuke by installing this package `Install-Package UnderTest.Nuke`
4. Add to the generated `Build.cs` file with the following:

```csharp 
    Project AcceptanceTestsProject => Solution.GetProject("AcceptanceTests");

    public Target AcceptanceTests =>
      _ =>
        _.DependsOn(UnitTests)
          .Executes(() =>
          {
            UnderTestTasks.UnderTestRunSuite(x => x
              .SetConfiguration(Configuration)
              .SetProject(AcceptanceTestsProject)
              .SetOnlyRunTheseTags(new[] {"@smoke"}));
          });
```
- where `AcceptanceTests` is the name of your UnderTest project
