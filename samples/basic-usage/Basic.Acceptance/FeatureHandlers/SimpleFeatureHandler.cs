using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnderTest;
using UnderTest.Attributes;

namespace Basic.Acceptance.FeatureHandlers
{
  [PublicAPI]
  [ExcludeFromCodeCoverage]
  [HandlesFeature("SimpleFeature.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [Given("Something exists")]
    public void SomethingExists()
    {
      Log.Information("    Something exists");
    }

    [When("Something happens")]
    public void SomethingHappens()
    {
      Log.Information("    something happens");
    }

    [Then("This should now be true")]
    public void ThisExists()
    {
      Log.Information("    this is true");
    }
  }
}
