using System.Collections.Generic;
using Nuke.Common;
using Nuke.Common.Execution;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Utilities.Collections;
using UnderTest.Nuke;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

[CheckBuildProjectConfigurations]
[UnsetVisualStudioEnvironmentVariables]
class Build : NukeBuild
{
  public static int Main() => Execute<Build>(x => x.AcceptanceTests);

  [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
  readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

  [Solution(@"samples\basic-usage\Basic.sln")]
  readonly Solution Solution;

  AbsolutePath SourceDirectory => RootDirectory / "src";

  Project AcceptanceTestsProject => Solution.GetProject("Basic.Acceptance");

  Target Clean => _ => _
    .Before(Restore)
    .Executes(() =>
    {
      SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
    });

  Target Restore => _ => _
    .Executes(() =>
    {
      DotNetRestore(s => s
        .SetProjectFile(Solution));
    });

  Target Compile => _ => _
    .DependsOn(Restore)
    .Executes(() =>
    {
      DotNetBuild(s => s
        .SetNoRestore(InvokedTargets.Contains(Restore))
        .SetProjectFile(Solution)
        .SetConfiguration(Configuration));
    });

  public Target AcceptanceTests =>
    _ =>
      _.DependsOn(Compile)
        .Executes(() =>
        {
          UnderTestTasks.UnderTestRunSuite(x => x
            .SetConfiguration(Configuration)
            .SetProject(AcceptanceTestsProject));
        });
}
