# Contributing

UnderTest is a free open-source project, with the goal that as many individuals as possible gain a benefit. We expect everyone to follow our [code of conduct](CODE_OF_CONDUCT.md), and it is important to understand [our philosophy](https://nuke.build/docs/getting-started/philosophy.html). 

We appreciate any kind of contribution that helps to grow the project. Following next, we would like to outline a few possibilities, expectations and thoughts.

- [Star the project](https://gitlab.com/under-test/undertest.nuke), especially when requesting features
- Follow us on [Twitter](https://twitter.com/UnderTestOSS), join our [Slack workspace](https://slofile.com/slack/undertestoss) 
- [Tweets](https://twitter.com/intent/tweet?text=UnderTest%20is%20awesome!%20https%3A%2F%2Fundertest.io), blog posts and other are very motivating
- Well-formed feedback is what shapes the technical future of the project
- [Issues are triaged](https://gitlab.com/under-test/undertest.nuke/issues) carefully; make sure to include all important information, like _expected_ vs. _actual_ behavior, version numbers etc.
- Pull-requests are much appreciated, but for trivial changes it's recommended to get in touch with the maintainers first
- Pull-requests must be started off from the active release `release-X.X.X` and should only use `git merge`, i.e. not `git rebase`
